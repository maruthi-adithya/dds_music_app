# music-app
Music library application using Django Framework


To install dependencies from [requirements.txt](https://github.com/madhuspot/polls-app/blob/master/requirements.txt): <br>
`pipenv install -r requirements.txt`

<hr>

Here's a more verbose desciption of setup of the project:
- pipenv for virtual environment management <br>
`pip3 install pipenv`

- to create a virtual env using pipenv <br>
`pipenv shell`

- install django <br>
`pipenv install django`

- install postgres from [here](https://postgresapp.com/).

- install psycopg2 (for postgres support) <br>
`pipenv install psycopg2` | [Here's a useful thread for macOS users](https://stackoverflow.com/a/24684701/5974048 )

- create a database (for this project it's `dds_music_app`) and update the follwing details in `dds_music_app/settings.py`<br>
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'dds_music_app',
        'USER': 'postgres',
        'PASSWORD': 'your-password-here', 
        'HOST': 'localhost'
    }
}
```

- make necessary migrations <br>
`python manage.py makemigrations` <br>
`python manage.py migrate`

- run the server <br>
`python manage.py runserver`

<hr>

# To-do
- add functionality to Favourite the songs / from Search page / from Album page

