from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from albums.models import Song, Album

# To-do!
def song_info(request, song_id):
    return render()

def search_song(request):
    queryset_list = Song.objects.all()
    # Keywords
    if 'keywords' in request.GET:
        print("Keywords :", request.GET['keywords'])
        keywords = request.GET['keywords']
        if keywords:
            queryset_list = queryset_list.filter(name__icontains=keywords)
        else:
            print('No keywords')
   

    context = {
        'songs': queryset_list,
        'values':request.GET
    }

    return render(request, 'songs/search_song.html', context)

def song_info(request, song_id):
    song = get_object_or_404(Song, pk=song_id)
    context = {
        'song': song
    }
    return render(request, 'songs/song_info.html', context)
