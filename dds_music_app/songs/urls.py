from django.urls import path

from . import views

urlpatterns = [
    path('search_song', views.search_song, name='search_song'),
    path('<int:song_id>', views.song_info, name='song_info'),
]