from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from albums.models import Song, Album

def index(request):
    albums = Album.objects.order_by('-date_published')[:3]

    context = {
        'albums':albums
    }

    return render(request, 'pages/index.html', context)

def about(request):
    return render(request, 'pages/about.html')

