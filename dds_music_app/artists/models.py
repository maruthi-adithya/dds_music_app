from django.db import models
from datetime import datetime

class Artist(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='artist_photos')
    description = models.TextField(blank=True)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    
    # to set the main field
    def __str__(self):
        return self.name
