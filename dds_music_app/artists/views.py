from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Artist

def artist_info(request, artist_id):
    artist = get_object_or_404(Artist, pk=artist_id)
    context = {
        'artist': artist
    }
    return render(request, 'artists/artist_info.html', context);


