from django.urls import path

from . import views

urlpatterns = [
    path('<int:album_id>', views.album_info, name='album_info'),
]