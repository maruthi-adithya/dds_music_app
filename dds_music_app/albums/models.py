from django.db import models
from datetime import datetime
from artists.models import Artist

class Album(models.Model):
    name = models.CharField(max_length=200)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE)
    album_cover = models.ImageField(upload_to='album_covers')
    date_published = models.DateTimeField(default=datetime.now, blank=True)
    genre = models.CharField(max_length=200, default='pop')

    def __str__(self):
        return self.name

class Song(models.Model):
    name = models.CharField(max_length=200)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
    file = models.FileField(upload_to='songs/')
    
    def __str__(self):
        return self.name
