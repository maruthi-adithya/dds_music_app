from django.shortcuts import render, get_object_or_404
from .models import Album, Song

def index(request):
    albums = Album.objects.order_by('-date_published')

    context = {
        'albums':albums
    }
    return render(request, 'albums/albums.html', context)

def album_info(request, album_id):
    album = get_object_or_404(Album, pk=album_id)
    context = {
        'album': album
    }
    return render(request, 'albums/album_info.html', context)


