from django.contrib import admin
from .models import Album, Song

class SongInline(admin.TabularInline):
    model = Song
    extra = 2


class AlbumAdmin(admin.ModelAdmin):
    inlines = [SongInline]

admin.site.register(Album, AlbumAdmin)
